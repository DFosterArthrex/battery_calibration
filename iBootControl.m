function out = iBootControl(IP,outlets,inputCommand)
out = 0;
%outlets is 8 element vector 0 = don't change, 1 = do command

switch inputCommand
    case 'OFF'
        charCommand = 'E';
    case 'ON'
        charCommand = 'D';
    otherwise
        charCommand = 'O';
end
actualCommand = cell(1);
if(min(outlets == 1)) %If all outlets use 'A'
    actualCommand{1} = [27 double(sprintf('admin')) 27 double('A') double(charCommand)];
else
    list = find(outlets);
    for i = 1:length(list)
        actualCommand{i} = [27 double(sprintf('admin')) 27 double(sprintf('%d',list(i))) double(charCommand)];
    end
end

portCmd = 9100;
buffSize = 10000;

tCmd = tcpip(IP, portCmd);
set(tCmd,'InputBufferSize',buffSize);
set(tCmd,'OutputBufferSize',buffSize);

if(~isempty(actualCommand))
    for i = 1:length(actualCommand)
        fopen(tCmd);
        fprintf(tCmd, actualCommand{i});
        fclose(tCmd);
    end
end

delete(tCmd);
