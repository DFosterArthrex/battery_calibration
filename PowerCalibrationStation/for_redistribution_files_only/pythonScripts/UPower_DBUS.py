#!/usr/bin/env python3
 
from pydbus import SystemBus

outputlist = [""]*36 #create a list that will be passed to each function and store a value


#def readUPowerDBUS(deviceName)
bus = SystemBus()
upower = bus.get("org.freedesktop.UPower")
device = upower.EnumerateDevices()
batmon1 = bus.get("org.freedesktop.UPower", device[1]) #sbs-2-000b
batmon2 = bus.get("org.freedesktop.UPower", device[0]) #sbs-2-0009

# print("Battery 1 percentage: {}".format(batmon1.Percentage))
# print("Battery 1 voltage: {}".format(batmon1.Voltage))
# print("Battery 1 state: {}".format(batmon1.State))
# print("Battery 1 capacity: {}".format(batmon1.Capacity))


def write_controller(fname):
    fd = open(fname,"w")
    fd.write("0x0071")
    fd.close()

def read_cell(fname):
    outlist = []
    fd = open(fname,"rb")
    for i in range(1,5):
        voltages = fd.read(2)
        cell_voltage = int.from_bytes(voltages,byteorder="little")
        # print(pack_name+" Cell "+str(i)+": " + str(cell_voltage)+ " mV")
        outlist.append("{}".format(str(cell_voltage)))
    fd.close()
    return outlist

def read_parameter(fname):
    fd = open(fname,"r")
    output = fd.read()
    # print(pack_name + " " + parameter + ": " + output.replace("\n",''))
    output = "{}".format(output.replace("\n",''))
    if output != "0" and len(output) > 1 and (output.isdigit() or output.replace("-","").isdigit()):
        output = output[0:len(output)-3]
    fd.close()
    return output

def read_charge_percent(fname):
    with open(fname,'r') as f:
        return f.read().strip()

def read_temp(fname):
    with open(fname,'r') as f:
        temp = int(f.read().strip())/10
        return '%3.1f'%temp
#build the output list of battery 1 info
try:
    outputlist[0] = read_temp("/sys/class/power_supply/" + batmon1.NativePath + "/temp") #read temperature
    outputlist[1] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/voltage_now") #read total voltage
    outputlist[2] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/charge_now") #read remaining charge capacity
    outputlist[3] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/charge_full") #read full charge capacity
    outputlist[4] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/current_now") #read current
    outputlist[5] = read_charge_percent("/sys/class/power_supply/" + batmon1.NativePath + "/capacity")
    write_controller("/sys/class/power_supply/" + batmon1.NativePath + "/reg_address") #read cells
    cell_list = read_cell("/sys/class/power_supply/" + batmon1.NativePath + "/reg_block")
    outputlist[6] = cell_list[0]
    outputlist[7] = cell_list[1]
    outputlist[8] = cell_list[2]
    outputlist[9] = cell_list[3]  
    outputlist[10] = "{}".format(batmon1.NativePath) #read battery pack name
    outputlist[11] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/status") #read charging status
except:
    for i in range(11):
        outputlist[i] = ""

# read_parameter("/sys/class/power_supply/sbs-2-000b/capacity",) #read capacity
# read_parameter("/sys/class/power_supply/sbs-2-000b/capacity_level") #read capacity level

#build the output list of battery 2 info
try:
    outputlist[12] = read_temp("/sys/class/power_supply/" + batmon2.NativePath + "/temp") #read temperature
    outputlist[13] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/voltage_now") #read total voltage
    outputlist[14] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/charge_now") #read remaining charge capacity
    outputlist[15] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/charge_full") #read full charge capacity
    outputlist[16] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/current_now") #read current
    outputlist[17] = read_charge_percent("/sys/class/power_supply/" + batmon2.NativePath + "/capacity")
    write_controller("/sys/class/power_supply/" + batmon2.NativePath + "/reg_address") #read cells
    cell_list = read_cell("/sys/class/power_supply/" + batmon2.NativePath + "/reg_block")
    outputlist[18] = cell_list[0]
    outputlist[19] = cell_list[1]
    outputlist[20] = cell_list[2]
    outputlist[21] = cell_list[3]  
    outputlist[22] = "{}".format(batmon2.NativePath) #read battery pack name
    outputlist[23] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/status") #read charging status
except:
    for i in range(11):
        outputlist[12+i] = ""

try:
    outputlist[24] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/cycle_count")
    outputlist[25] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/cycle_count")
    outputlist[26] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/time_to_empty_avg")
    outputlist[27] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/time_to_empty_avg")
    outputlist[28] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/time_to_full_avg")
    outputlist[29] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/time_to_full_avg")
    outputlist[30] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/capacity_level")
    outputlist[31] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/capacity_level")
    outputlist[32] = read_parameter("/sys/class/power_supply/" + batmon1.NativePath + "/health")
    outputlist[33] = read_parameter("/sys/class/power_supply/" + batmon2.NativePath + "/health")
except:
    for i in range(12):
        outputlist[24+i] = ""

outputString = ",".join(outputlist) #turns list into a comma seperated string that can be saved into a csv file
print(outputString)


