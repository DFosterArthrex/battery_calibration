import os
# import threading
from datetime import datetime
import subprocess
import csv
import time
import serial
from BKPrecision1685BControl import toggle_powersupply
from arduino_serial_write import toggle_power_switch
import sys

headerstr = ["Time", "Temperature1", "Voltage1", "RemCap1", "FullChgCap1", "AvgCurrent1", "StateofCharge1", "Cell1Voltage1", "Cell2Voltage1", "Cell3Voltage1", "Cell4Voltage1","packName1","status1", "Temperature2", "Voltage2", "RemCap2", "FullChgCap2", "AvgCurrent2", "StateofCharge2", "Cell1Voltage2", "Cell2Voltage2", "Cell3Voltage2", "Cell4Voltage2","packName2","status2"]
outputfolder = "/battery_logs"

charge_level_bat1 = 100
charge_level_bat2 = 100
error_count = 0

def nano_battery_logger(charge_status):
	global i
	global charge_level_bat1
	global charge_level_bat2
	global outfilename
	global t
	global error_count
	charge_flag = charge_status #0 = discharge 1 = charge
	
	# t = threading.Timer(3.0, nano_battery_logger)
	# t.start()
	
	# os.system("plink -ssh root@192.168.0.1 ""python3 /tmp/UPower_DBUS.py"" >> output.csv")
	# os.system("plink -ssh root@192.168.0.1 ""python3 /tmp/UPower_DBUS.py")
	proc=subprocess.Popen("plink -ssh root@192.168.0.1 ""python3 /tmp/UPower_DBUS.py", shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE )
	(output,err_data)=proc.communicate()
	if "FATAL ERROR" in str(err_data):
		error_count += 1
		print("output:" + str(output))
		print("error data:" + str(err_data))
		print("error count = " + str(error_count))
		if error_count >= 5 or (charge_flag == 0 and (float(charge_level_bat1) <= 5 or float(charge_level_bat2) <= 5)):
			print("charge_flag0 = " + str(charge_flag))
			print("charge level bat1 {} charge level bat2 {}".format(float(charge_level_bat1), float(charge_level_bat2)))
			return 0
	else:
		out = output.decode("utf-8")
		out = out.replace("\n","")
		out_list = out.split(",")
		# print(out_list)
		# print(out_list[5])

		out_list = [datetime.now()] + out_list
		write_to_CSV(out_list, outfilename)

		i = i + 1
		print("logging-"+str(i)+"-"+str(datetime.now()))
		if out_list[6] and out_list[18]:
			charge_level_bat1 = out_list[6]
			charge_level_bat2 = out_list[18]
		error_count = 0

		if charge_flag == 1 and out_list[12] == "Full" and out_list[24] == "Full":
			print("charge_flag1 = " + str(charge_flag))
			return 0

	return 1

def write_to_CSV(listname, filename):
	fid = open(filename,'a', newline='')
	wr = csv.writer(fid, quoting=csv.QUOTE_ALL)
	wr.writerow(listname)
	fid.close()

#########################################################
#main logging program
if __name__ == "__main__":
	
	numLoops = -1

	if len(sys.argv) > 1:	
		numLoops = sys.argv[1]

	power_supply_com = 'COM21'
	arduino_com = 'COM67'

	cyclecount = 0
	while(cyclecount != int(numLoops)):
		outfileTimestamp = time.strftime("%Y%m%d-%H%M%S")
		outputdir = os.getcwd() + outputfolder + "/Profile-" +  outfileTimestamp
		if not os.path.exists(outputdir):
		    os.makedirs(outputdir)
		#write the CSV header with timestamped filename for discharge
		outfilename = outputdir + "/" + outfileTimestamp + "-BatteryDischarge.csv"
		write_to_CSV(headerstr, outfilename)

		#copy the UPower_DBUS reading program to the tmp dir
		os.system("pscp UPower_DBUS.py root@192.168.0.1:/tmp")

		#Start Logging the discharge
		i = 0
		while(1):
			if i == 1:
				#Start discharge (power supply off if charge == 100% and current == 0)
				toggle_powersupply(power_supply_com,'off')
				print("Power Supply OFF")
			status = nano_battery_logger(0)
			if status == 0:
				break
			time.sleep(3)

		print("end of discharge test... waiting for 20 minutes before charging...")
		time.sleep(60*20) #remain off for x mins


		#write the CSV header with timestamped filename for 
		outfilename = outputdir + "/" + outfileTimestamp + "-BatteryCharge.csv"
		write_to_CSV(headerstr, outfilename)

		#Start charging (power supply on)
		toggle_powersupply(power_supply_com,'on')
		print("Power Supply ON")
		time.sleep(60)

		#turn on Nano CCU
		toggle_power_switch(arduino_com)
		time.sleep(120) #wait for software to boot up

		#copy the UPower_DBUS reading program to the tmp dir
		os.system("pscp UPower_DBUS.py root@192.168.0.1:/tmp")

		#Start Logging the charge
		#Start Logging the discharge
		i = 0
		while(1):
			status = nano_battery_logger(1)
			if status == 0:
				break
			time.sleep(3)

		print("end of Charge test... waiting for 20 minutes before discharging...")
		time.sleep(60*20) #remain off for x mins
		cyclecount = cyclecount + 1