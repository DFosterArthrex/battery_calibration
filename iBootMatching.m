function devices = iBootMatching(devices,iBootIP,hostname)

for i = 1:length(iBootIP) %Sequentially turn off outlets and read power of devices.
    turnOffPower(1:4,iBootIP{i});
end
pause(2.5);
for j=1:length(devices)
    try
        current_now = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/current_now');
        current_now = str2double(current_now);
        if current_now < 0
            devices(j).outlet = 4;
        else
            devices(j).outlet = 8;
        end
    catch
        devices(j).outlet = 0;
    end
end
for i = 1:length(iBootIP)
    turnOnPower(1:8,iBootIP{i});
end

for i = 1:length(iBootIP) %Sequentially turn off outlets and read power of devices.
    turnOffPower([1 2 5 6],iBootIP{i});
end
pause(2.5);
for j=1:length(devices)
    try
        current_now = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/current_now');
        current_now = str2double(current_now);
        if current_now < 0
            if devices(j).outlet == 4
                devices(j).outlet = 2;
            elseif devices(j).outlet == 8
                devices(j).outlet = 6;
            end
        end
    catch
        devices(j).outlet = 0;
    end
end
for i = 1:length(iBootIP)
    turnOnPower(1:8,iBootIP{i});
end

for i = 1:length(iBootIP) %Sequentially turn off outlets and read power of devices.
    turnOffPower([1 3 5 7],iBootIP{i});
end
pause(2.5);
for j=1:length(devices)
    try
        current_now = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/current_now');
        current_now = str2double(current_now);
        if current_now < 0
            if devices(j).outlet == 2
                devices(j).outlet = 1;
            elseif devices(j).outlet == 4
                devices(j).outlet = 3;
            elseif devices(j).outlet == 6
                devices(j).outlet = 5;
            elseif devices(j).outlet == 8
                devices(j).outlet = 7;
            end
        end
    catch
        devices(j).outlet = 0;
    end
end

for i = 1:length(iBootIP)
    turnOnPower(1:8,iBootIP{i});
end

[~,OutletIndex]=sort([devices.outlet]);
for i = 1:length(iBootIP) %Sequentially turn off outlets and read power of devices.
    turnOffPower(1:8,iBootIP{i});
    pause(2.5);
    for j=OutletIndex
        try
            current_now = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/current_now');
            current_now = str2double(current_now);
            if current_now < 0
                devices(j).iBootIP = iBootIP{i};
                fprintf('Device %s to iBootIP %s Outlet %i\r\n',hostname(j),devices(j).iBootIP,devices(j).outlet);
            end
        catch
        end
    end
    turnOnPower(1:8,iBootIP{i});
end

for j=1:length(devices)
    if strcmp(devices(j).iBootIP,'unMatched')
        devices(j).outlet = 0;
    end
end

end