close all

current_now = figure('Name','current_now');
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.current_now])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.current_now])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);