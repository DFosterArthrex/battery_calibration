function [IP, hostname] = SimpleDiscover3(num_to_discover)
IP = [];
timeout = 200;
startTic = tic;
NET.addAssembly([ShowPath '\ResolveNanoHosts.dll']);
while length(IP)<num_to_discover && toc(startTic)<timeout
    [~, IPlist] = ResolveNanoHosts.ResolveNanoHosts.FindIPAddresses('192.168.0.','f8-dc-7a',0.1);
    IP = IPlist.string;
    pause(5)
end
timeoutBool = toc(startTic)>=timeout;
hostname = strings(1,length(IP));
for j = 1:length(IP)
    NanoCon = NanoSSH(IP(j),'arthrex','Arthrex1');
    startTic2 = tic;
    while hostname(j)=="" && toc(startTic2)<20
        try
            out = NanoCon.RunCommand('act-mfg-eeprom display');
            SN = regexp(out,'\nserial=(?<SerialNumber>\S*)','names');
            hostname(j) = string(SN.SerialNumber);
        catch
        end
    end
end
if timeoutBool
    uiwait(errordlg(sprintf('Device Matching Timeout Check Connections+Device Power and Restart Program \n%i Devices Found: \n%s',length(IP),strjoin(hostname,'\n'))))
    error('Device Matching Timeout Check Connections+Device Power and Restart Program \n%i Devices Found: \n%s',length(IP),strjoin(hostname,'\n'));
end
if num_to_discover < length(IP)
    num_of_devices=inputdlg(sprintf('Number of Devices Detected:%i Exceeds Number Entered:%i\nPlease Verify Connections and Reenter Number of Devices Connected\nEnter number of devices between 1-20',length(IP),num_to_discover),...
        'Input',[1 60],{sprintf('%i',length(IP))});
    while str2double(num_of_devices)>20 || str2double(num_of_devices)<1 || isnan(str2double(num_of_devices))
        num_of_devices=inputdlg(sprintf('Number of Devices Detected:%i Exceeds Number Entered:%i\nPlease Verify Connections and Reenter Number of Devices Connected\nEnter number of devices between 1-20',length(IP),num_to_discover),...
            'Input',[1 60],{sprintf('%i',length(IP))});
    end
    [IP, hostname] = SimpleDiscover3(str2double(num_of_devices));
end

end

