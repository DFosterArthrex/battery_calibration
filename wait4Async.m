function output = wait4Async(input,maxTime)
%input can be 1 or more NanoSSH objects, waits untill all Asyncronus
%commands are done ore maxTime is reached.

start = tic;

done = zeros(1,length(input));

while(~min(done) && toc(start) < maxTime)
    pause(0.1)
    for i = 1:length(input)
        done(i) = input(i).CheckCommandAsync();
    end
end
if toc(start) > maxTime
    warning('Wait4async timeout')
end
output = toc(start);