function output = UploadScripts(NanoCon)

%Upload scripts to /tmp/

localScriptDir = [ShowPath '\pythonScripts\'];

list = dir(localScriptDir);

output.ScriptDir = '/tmp/';

%Make Directory
% out = NanoCon.RunCommand(['mkdir ' output.ScriptDir]);

for i = 1:length(list)
    if(length(list(i).name) > 2)
        out = NanoCon.ScpUpload([localScriptDir list(i).name], [output.ScriptDir list(i).name]);
        %Verify file is there
        lsout = NanoCon.RunCommand(['ls ' output.ScriptDir list(i).name]);
        output.(matlab.lang.makeValidName(list(i).name)) = ~contains(lsout,'cannot access') && ~isempty(lsout);
    end
end

