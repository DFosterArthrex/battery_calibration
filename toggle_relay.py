import serial
import time

#serial com with an arduino/relay board
#arduino program is looking for a serial char to indicate the port/state
#1 = Port 1 off, 2 = Port 1 on, 3 = Port 2 off ... 8 = port 4 on
def toggle_relay(COM_Port, relay_number,state):
	ser = serial.Serial(
	    port=COM_Port,
	    baudrate=9600,
	)
	ser.close()         # close port
	time.sleep(1)

	if(ser.isOpen() == False):
	    ser.open()
	    time.sleep(2)

	if state == 'on' or state == 'ON':
		ser.write(str(relay_number*2).encode('UTF-8'))      # Port x ON
	elif state == 'off' or state == 'OFF':
		ser.write(str((relay_number*2)-1).encode('UTF-8'))      # Port x OFF
	ser.close()         # close port


# toggle_relay('COM19',3,'on')
# time.sleep(2)
# toggle_relay('COM19',3,'off')