close all
%% Results
for j=1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    Bat1charge_full = [Bat1.charge_full];
    Bat2charge_full = [Bat2.charge_full];
%     Bat1charge_full(1) = Bat1charge_full(end);
    shipMode = 0;
    if (Bat1charge_full(1) == Bat1charge_full(end)) || (Bat2charge_full(1) == Bat2charge_full(end))
        status='<b><font color="red">WARNING</b></font> Initial Charge Capacity = Final Charge Capacity<br><br>';
        if Finished(j) && shipMode
            if shipModeFail(j)
                status=[status ' <b><font color="red">Failed To Go Into Ship Mode</b></font>'];
                progress = 1;
            else
                status=[status ' <b><font color="green">Ship Mode</b></font>'];
                progress = 1;
            end
        elseif Finished(j) && ~shipMode
            status = [status ' <b><font color="green">Default Mode</b></font>'];
            progress = 1;
        end
    elseif Finished(j) && shipMode
        if shipModeFail(j)
            status='<b><font color="green">Calibrated</b></font>, <b><font color="red">Failed To Go Into Ship Mode</b></font>';
            progress = 1;
        else
            status='<b><font color="green">Calibrated</b></font>, <b><font color="green">Ship Mode</b></font>';
            progress = 1;
        end
    elseif Finished(j) && ~shipMode
        status = '<b><font color="green">Calibrated</b></font> <b><font color="green">Ship Mode</b></font>';
        progress = 1;
    end
    
    a = sprintf(['<html><span style="font-size:45px">'...
        '<br><b>%s</b> %s <br><br> BAT1 Initial Charge Capacity = <b>%g</b>; BAT1 Calibrated Charge Capacity = <b>%g</b><br><br>BAT2 Initial Charge Capacity = <b>%g</b> BAT2 Calibrated Charge Capacity = <b>%g</b>' ...
        '<br><br></span></html>'],...
        hostname(j),status,Bat1charge_full(1),Bat2charge_full(1),Bat1charge_full(end),Bat1charge_full(end));
    disp(a)
end
 %% Cell Voltages
figure('Position', [10 10 1280 720]);
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.cell_1_voltage], [Bat1.time], [Bat1.cell_2_voltage],[Bat1.time], [Bat1.cell_3_voltage],[Bat1.time], [Bat1.cell_4_voltage])
    legend({'cell 1 voltage','cell 2 voltage','cell 3 voltage','cell 4 voltage'},'Location','southwest','NumColumns',2)
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.cell_1_voltage], [Bat2.time], [Bat2.cell_2_voltage],[Bat2.time], [Bat2.cell_3_voltage],[Bat2.time], [Bat2.cell_4_voltage])
    legend({'cell 1 voltage','cell 2 voltage','cell 3 voltage','cell 4 voltage'},'Location','southwest','NumColumns',2)
    title(sprintf('%s Bat2',hostname(j)));
end

allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);

 %% Charge Percent
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.charge_percent])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.charge_percent])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);

%% Charge
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.charge_now])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.charge_now])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);

%% Charge Full
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.charge_full])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.charge_full])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);

%% Capacity
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.capacity])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.capacity])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);
%% Temperature
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.temp])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.temp])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);

%% Current
for j =1:length(devices)
    Bat1 = [devices(j).out.Bat1];
    Bat2 = [devices(j).out.Bat2];
    AX(2*j-1) = subplot(length(devices),2,2*j-1);
    plot([Bat1.time], [Bat1.current_now])
    title(sprintf('%s Bat1',hostname(j)));
    
    AX(2*j) = subplot(length(devices),2,2*j);
    plot([Bat2.time], [Bat2.current_now])
    title(sprintf('%s Bat2',hostname(j)));
end
allYLim = get(AX, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AX, 'YLim', [min(allYLim), max(allYLim)]);

allXLim = get(AX, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AX, 'XLim', [min(allXLim), max(allXLim)]);
