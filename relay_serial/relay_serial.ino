int analogPin = 3;
int val = 0;

void setup(){
  Serial.begin(9600);

  //Set all the pins we need to output pins
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(6, HIGH);
  digitalWrite(7, HIGH);
  digitalWrite(8, HIGH);
  digitalWrite(9, HIGH);
  
}

void loop (){

  val = analogRead(analogPin);
  
  if (Serial.available()) {

    //read serial as a character
    char ser = Serial.read();
    
//    char switchState = Serial.read();
//    Serial.print(switchNum);
//    Serial.print(switchState);


    //NOTE because the serial is read as "char" and not "int", the read value must be compared to character numbers
    //hence the quotes around the numbers in the case statement
    switch (ser) {
      case '1':
        triggerPin(6,HIGH);
        break;
      case '2':
        triggerPin(6,LOW);
        break;
      case '3':
        triggerPin(7,HIGH);
        break;
      case '4':
        triggerPin(7,LOW);
        break;
      case '5':
        triggerPin(8,HIGH);
        break;
      case '6':
        triggerPin(8,LOW);
        break;
      case '7':
        triggerPin(9,HIGH);
        break;
      case '8':
        triggerPin(9,LOW);
        break;
      case '9':
        Serial.print(val);
        break;    
    } 
  }
}

void triggerPin(int pin, int state){
  digitalWrite(pin, state);
//  Serial.print("Pin: ");
//  Serial.print(pin);
//  Serial.print("high\n");
//  delay(1000);
//  digitalWrite(pin, st);
//  Serial.print("Pin: ");
//  Serial.print(pin);
//  Serial.print("low\n");
//  delay(1000);
}
