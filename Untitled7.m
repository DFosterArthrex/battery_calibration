StartupTime = now;
ArchiveLocation = [pwd '\ShipModeTestArchive\' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '\'];
if(~exist(ArchiveLocation,'dir'))
    mkdir(ArchiveLocation)
end
[IP,hostname] = findIPAddress2;
for j = 1:length(devices)
    %     devices(j).Connection.RunCommand('/usr/sbin/battery-ship-mode')
    %     pause(1)
    DownloadBootlog(devices(j).Connection,sprintf('%s Device %s.txt', ArchiveLocation, hostname(j)));
    fid = fopen(sprintf('%s Device %s.txt', ArchiveLocation, hostname(j)));
    tempA = fread(fid,'*char')';
    fclose(fid);
    Result(j) = contains(tempA,'attempting to exit ship mode');
    passFail ={'FAIL' 'PASS'};
    fprintf('Result Device %s: %s \r\n',hostname(j),passFail{Result(j)+1});
    fid = fopen(sprintf('%sResult.txt', ArchiveLocation),'a');
    fprintf(fid,'Result Device %s: %s \r\n',hostname(j),passFail{Result(j)+1});
    fclose(fid);
    
    
    devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-0009/reg_address');
    devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-000b/reg_address');
    Bat1ShipMode = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block')
    Bat2ShipMode = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block')
end

