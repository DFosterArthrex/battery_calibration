function [IP, hostname] = SimpleDiscover2(num_to_discover)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
startTic = tic;
j=0;
hostname = strings(1,num_to_discover);
IP = strings(1,num_to_discover);
IP_range = ones(1,20);
timeout = 60 + num_to_discover*10;
while toc(startTic)<timeout  && j<num_to_discover
    for i = find(IP_range)
        [~, b] = system(sprintf('ping 192.168.0.%d -w 2 -n 1',i));
        pause(.2)
        if contains(b,'Reply')
            j = j+1;
            IP_range(i) = 0;
            IP(j) = sprintf('192.168.0.%d',i);
            NanoCon = NanoSSH(IP(j),'arthrex','Arthrex1');
            startTic2 = tic;
            while hostname(j)=="" && toc(startTic2)<(timeout/num_to_discover)
                try
                    out = NanoCon.RunCommand('act-mfg-eeprom display');
                    SN = regexp(out,'\nserial=(?<SerialNumber>\S*)','names');
                    hostname(j) = string(SN.SerialNumber);
                catch
                end
            end
            if j>=num_to_discover
                break;
            end
        end
    end
end
if toc(startTic)>=timeout
    errordlg(sprintf('Device Matching Timeout Check Connections+Device Power and Restart Program \nDevices Found: \n%s',strjoin(hostname,'\n')))
    error('Device Matching Timeout Check Connections+Device Power and Restart Program \nDevices Found: \n%s',strjoin(hostname,'\n'));
end
for i = find(IP_range)
    [~, b] = system(sprintf('ping 192.168.0.%d -w 2 -n 1',i));
    pause(.2)
    if contains(b,'Reply')
        num_of_devices=inputdlg(sprintf('Number of Devices Detected Exceeds Number Entered\nPlease Verify Connections and Reenter Number of Devices Connected\nEnter number of devices between 1-20'));
        while str2double(num_of_devices)>20 || str2double(num_of_devices)<1 || isnan(str2double(num_of_devices))
            num_of_devices=inputdlg(sprintf('Number of Devices Detected Exceeds Number Entered\nPlease Verify Connections and Reenter Number of Devices Connected\nEnter number of devices between 1-20'));
        end
        [IP, hostname] = SimpleDiscover2(str2double(num_of_devices));
    end
end
end

