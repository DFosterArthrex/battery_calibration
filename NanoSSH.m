classdef NanoSSH < handle
    properties
        IPAddress
        UserName
        PassWord
        Client
        SftpClient
        ScpClient
        Command
        Async
    end
    methods
        function obj = NanoSSH(IPAddress,UserName,PassWord)
            obj.IPAddress = IPAddress;
            obj.UserName = UserName;
            obj.PassWord = PassWord;
            
            %Try to automatically load the library
            try
                asmInfo = NET.addAssembly('System');
                
                asmInfo = NET.addAssembly('Renci.SshNet');
            catch err
                asmInfo = NET.addAssembly([ShowPath '\Renci.SshNet.dll']);
                ReportError(err)
            end
        end
        
        function asmInfo = loadLibrary(Path)
            asmInfo = NET.addAssembly(Path);
        end
        
        function Connect(obj)
            try
                obj.Client = Renci.SshNet.SshClient(obj.IPAddress,obj.UserName,obj.PassWord);
                obj.Client.ConnectionInfo.Timeout = System.TimeSpan.FromSeconds(2);
                obj.Client.Connect();
            catch err
                ReportError(err)
            end
        end
        
        function Disconnect(obj)
            try
                obj.Client.Disconnect();
            catch err
                ReportError(err)
            end
        end
        
        function output = TestRead(obj)            
            try
                if(isempty(obj.Client) || ~obj.Client.IsConnected)
                    obj.Client = Renci.SshNet.SshClient(obj.IPAddress,obj.UserName,obj.PassWord);
                    obj.Client.ConnectionInfo.Timeout = System.TimeSpan.FromSeconds(2);
                    obj.Client.Connect();
                end
                
                command = obj.Client.CreateCommand(sprintf('cat /osversion'));
                output = command.Execute().char;
                
                %tmp is a .NET string, need to convert it to char array,
                %then get rid of the preceding 0x
            catch
                output = [];
            end
        end
        
        function output = ReadRegister(obj,varargin)
            
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            if(~isempty(varargin))
                obj.SetAddress(varargin{1});
            end
            
            try
                command = obj.Client.CreateCommand(sprintf('echo ''%s'' | sudo -S sh -c "cat /sys/class/act/fpga/reg_data"',obj.PassWord));
                tmp = command.Execute();
                
                %tmp is a .NET string, need to convert it to char array,
                %then get rid of the preceding 0x
                tmp2 = tmp.char;
                output = tmp2(3:10);
            catch err
                ReportError(err)
            end
        end
        
        function output = WriteRegister(obj,data,varargin)
            
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            if(~isempty(varargin))
                obj.SetAddress(varargin{1});
            end
            
            try
                command = obj.Client.CreateCommand(sprintf('echo ''%s'' | sudo -S sh -c "echo 0x%s > /sys/class/act/fpga/reg_data"',obj.PassWord,data));
                output = command.Execute();
            catch err
                ReportError(err)
            end
        end
        
        function output = SetAddress(obj,addr)
            
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            try
                command = obj.Client.CreateCommand(sprintf('echo ''%s'' | sudo -S sh -c "echo %d > /sys/class/act/fpga/reg_address"',obj.PassWord,addr));
                output = command.Execute();
            catch err
                ReportError(err)
            end
        end
        
        function output = WriteBit(obj,data,loc,varargin)
            %data is binary!
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            if(~isempty(varargin))
                obj.SetAddress(varargin{1});
            end
            
            try
                r = ReadRegister(obj);
                
                b = fliplr(dec2bin(hex2dec(r),32));
                
                for i = 1:length(loc)
                    b(loc(i)+1) = num2str(data(i));
                end
                
                v = dec2hex(bin2dec(fliplr(b)),8);
                
                output = WriteRegister(obj,v);
            catch err
                ReportError(err)
            end
        end
        
        function output = ReadBit(obj,loc,varargin)
            %data is binary!
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            if(~isempty(varargin))
                obj.SetAddress(varargin{1});
            end
            
            try
                r = ReadRegister(obj);
                
                b = fliplr(dec2bin(hex2dec(r),32));
                
                output = b(loc+1);
            catch err
                ReportError(err)
            end
        end
        
        function output = RunCommand(obj,cmd)
            
            if(isempty(obj.Client) || ~obj.Client.IsConnected)
                obj.Connect();
            end
            
            try
                command = obj.Client.CreateCommand(sprintf('echo ''%s'' | sudo -S sh -c "%s"',obj.PassWord,cmd));
                tmp = command.Execute();
                output = tmp.char;
            catch err
                ReportError(err)
                output = 'BADDBEEF';
            end
        end
        
        function output = SftpConnect(obj)
            try
                obj.SftpClient = Renci.SshNet.SftpClient(obj.IPAddress,obj.UserName,obj.PassWord);
                
                obj.SftpClient.Connect();
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = SftpUpload(obj,sourceFilePath,targetFilePath)
            try
                if(isempty(obj.SftpClient) || ~obj.SftpClient.IsConnected)
                    obj.SftpConnect();
                end
                
                sourceFile = System.IO.File.OpenRead(sourceFilePath);
                %                 sourceFile = obj.SftpClient.OpenRead(sourceFilePath);
                
                obj.SftpClient.UploadFile(sourceFile, targetFilePath);
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = SftpDownload(obj,sourceFilePath,outputFilePath)
            try
                if(isempty(obj.SftpClient) || ~obj.SftpClient.IsConnected)
                    obj.SftpConnect();
                end
                
                outputFile = System.IO.File.OpenWrite(outputFilePath);
                %                 outputFile = obj.SftpClient.OpenWrite(outputFilePath);
                
                obj.SftpClient.DownloadFile(sourceFilePath, outputFile);
                
                outputFile.Close()
                
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = ScpConnect(obj)
            try
                obj.ScpClient = Renci.SshNet.ScpClient(obj.IPAddress,obj.UserName,obj.PassWord);
                
                obj.ScpClient.Connect();
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = ScpUpload(obj,sourceFilePath,targetFilePath)
            try
                if(isempty(obj.ScpClient) || ~obj.ScpClient.IsConnected)
                    obj.ScpConnect();
                end
                
                sourceFile = System.IO.File.OpenRead(sourceFilePath);
                %                 sourceFile = obj.ScpClient.OpenRead(sourceFilePath);
                
                obj.ScpClient.Upload(sourceFile, targetFilePath);
                sourceFile.Close();
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = ScpDownload(obj,sourceFilePath,outputFilePath)
            try
                if(isempty(obj.ScpClient) || ~obj.ScpClient.IsConnected)
                    obj.ScpConnect();
                end
                
                outputFile = System.IO.File.OpenWrite(outputFilePath);
                %                 outputFile = obj.ScpClient.OpenWrite(outputFilePath);
                
                obj.ScpClient.Download(sourceFilePath, outputFile);
                
                outputFile.Close()
                
                output = 1;
            catch err
                ReportError(err)
            end
        end
        
        function output = RunCommandAsync(obj,command)
            %Starts an Async Command
            try
                if(isempty(obj.Client) || ~obj.Client.IsConnected)
                    obj.Connect();
                end
                
                obj.Command = obj.Client.CreateCommand(sprintf('echo ''%s'' | sudo -S sh -c "%s"',obj.PassWord,command));
                obj.Async = obj.Command.BeginExecute();
                pause(0.05)
                if obj.Async.IsCompleted && ~obj.Async.BytesReceived
                    output = false;
                else
                    output = true;
                end
            catch err
                output = false;
                warning(ReportError(err))
            end
        end
        
        function output = CheckCommandAsync(obj)
            %Checks if Async Commmand is complete
            try
                if(isempty(obj.Client) || ~obj.Client.IsConnected)
                    obj.Connect();
                end
                
                output = isprop(obj,'Async') && obj.Async.IsCompleted;
            catch err
                output = false;
                warning(ReportError(err))
            end
        end
        
        function output = ReturnCommandAsync(obj)
            %Returns output of Async Command
            try
                if(isempty(obj.Client) || ~obj.Client.IsConnected)
                    obj.Connect();
                end
                if(~obj.CheckCommandAsync())
                    %Not Complete
                    warning('Command not complete')
                    output = [];
                    return
                end
                
                output = string(obj.Command.EndExecute(obj.Async));
            catch err
                output = false;
                warning(ReportError(err))
            end
        end
        
    end
end
