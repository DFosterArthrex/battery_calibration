function output = DownloadBootlog(NanoCon,localFileName)

%journalctl > /tmp/journal.log
NanoCon.RunCommand('journalctl > /tmp/journal.log');

NanoCon.ScpDownload('/tmp/journal.log',localFileName);

output = 1;
