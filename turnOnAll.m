iBootIP = readIBootIP'; %Reads /settings/iBootIP.txt to get iBoot IP's
for i = 1:length(iBootIP) %Turn on all iBoot bars prior to matching
    turnOnPower(1:8,iBootIP{i});
end

function turnOnPower(k,iBootIP)
outlets = zeros(1,8);
outlets(k) = 1;
iBootControl2(iBootIP,outlets,'ON');
end

function iBootIP = readIBootIP
if(exist([ShowPath,'\settings\iBootIP.txt'],'file'))
    outlets = importdata([ShowPath,'\settings\iBootIP.txt']);
    
    %field names on first line
    tmp = textscan(outlets{1},'%s');
    header = tmp{1};
    numFields = length(header);
    expresion = [sprintf('(?<%s>\\S*)\\s*',header{1:end-1}) sprintf('!(?<%s>(\\S*\\s*)*)',header{end})];
    IPAddress = [];
    %Print Outlet Info
    for i = 2:length(outlets)
        DevicesTmp = regexp(outlets{i},expresion,'names');
        Fields = fieldnames(DevicesTmp);
        for j = 1:length(Fields)
            Devices(i-1).(Fields{j}) = DevicesTmp.(Fields{j});
        end
        IPAddress = [IPAddress; Devices(i-1).IPAddress(1:end)];
        %     LogText(handles,sprintf('Device #%d',i-1))
        %     for j = 1:numFields
        %         LogText(handles,sprintf(' %15s: %-15s',header{j},handles.Devices(i-1).(header{j})))
        %     end
    end
    numDevices = cellstr(int2str(length(Devices)));
    iBootIP = cellstr(IPAddress);
end
end