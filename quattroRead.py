from serial import Serial
import datetime
import re

COM = 'COM3'
def readBytes():
    con.reset_input_buffer()
    while con.read(1) is not b'{': pass
    return con.read(75)

def decodeBytes():
    result = None
    while True:
        try:
            r = readBytes() 
            assert r[-1] is ord('}') and len(r) is 75, 'read failed'
            ints = [byte & 0b0111_1111 for byte in r[:-1]]
            cell = [0] * 5
            cell[0] = datetime.datetime.now().isoformat()
            for i in range(4):
                cell[i+1] = ints[44 + 2*i] + ints[45 + 2*i] / 100
            return cell
        except Exception as err:
            print(err)

file = datetime.datetime.now().isoformat()
file = re.sub(':', '_', file)
file = file.split('.')[0]
file = f"{file}.csv"
con = Serial(COM, timeout=5)
with open(file, "a") as csvfile:
    file.write('time, bat_cell1, bat_cell2, bat_cell3, bat_cell4\n')
while True:
    cell = decodeBytes()
    print(cell)
    with open(file, 'a') as csvfile:
        file.write(f'{cell[0]}, {cell[1]}, {cell[2]}, {cell[3]}, {cell[4]}\n')
