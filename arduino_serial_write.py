import serial
import time


def toggle_power_switch(com_port):
	ser = serial.Serial(
	    port=com_port,
	    baudrate=9600,
	)
	ser.close()         # close port
	time.sleep(1)

	if(ser.isOpen() == False):
	    ser.open()
	    print('openning arduino port')
	time.sleep(2)

	ser.write(b'2000')
	ser.close()         # close port