function [Bat1,Bat2] = ReadPowerAsync(NanoCon)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
outStr = strip(NanoCon.ReturnCommandAsync);
out = textscan(outStr,'%d%d%d%d%d%d%d%d%d%d%s%s%d%d%d%d%d%d%d%d%d%d%s%s%d%d%d%d%d%d%s%s%s%s','Delimiter',',');
Bat1.time = datetime('now');
Bat1.temp = out{1};
Bat1.voltage_now = out{2};
Bat1.charge_now = out{3};
Bat1.charge_full = out{4};
Bat1.current_now = out{5};
Bat1.charge_percent = out{6};
Bat1.cell_1_voltage = out{7};
Bat1.cell_2_voltage = out{8};
Bat1.cell_3_voltage = out{9};
Bat1.cell_4_voltage = out{10};
Bat1.name = string(out{11}{1});
Bat1.status = string(out{12}{1});

Bat2.time = Bat1.time;
Bat2.temp = out{12+1};
Bat2.voltage_now = out{12+2};
Bat2.charge_now = out{12+3};
Bat2.charge_full = out{12+4};
Bat2.current_now = out{12+5};
Bat2.charge_percent = out{12+6};
Bat2.cell_1_voltage = out{12+7};
Bat2.cell_2_voltage = out{12+8};
Bat2.cell_3_voltage = out{12+9};
Bat2.cell_4_voltage = out{12+10};
Bat2.name = string(out{12+11}{1});
Bat2.status = string(out{12+12}{1});

Bat1.cycle_count = out{24+1};
Bat2.cycle_count = out{24+2};
Bat1.time_to_empty_avg = out{24+3};
Bat2.time_to_empty_avg = out{24+4};
Bat1.time_to_full_avg = out{24+5};
Bat2.time_to_full_avg = out{24+6};
Bat1.capacity_level = string(out{24+7}{1});
Bat2.capacity_level = string(out{24+8}{1});
Bat1.health = string(out{24+9}{1});
Bat2.health = string(out{24+10}{1});

NanoCon.RunCommand('echo 0x56 > /sys/class/power_supply/sbs-2-000b/reg_address');
NanoCon.RunCommand('echo 0x56 > /sys/class/power_supply/sbs-2-0009/reg_address');
Bat1.Reg56 = strip(string(NanoCon.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block')));
Bat2.Reg56 = strip(string(NanoCon.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block')));

NanoCon.RunCommand('echo 0x53 > /sys/class/power_supply/sbs-2-000b/reg_address'); %permenant failure status
NanoCon.RunCommand('echo 0x53 > /sys/class/power_supply/sbs-2-0009/reg_address'); %permenant failure status
Bat1.PFReg = strip(string(NanoCon.RunCommand('xxd -p /sys/class/power_supply/sbs-2-000b/reg_block'))); %permenant failure status
Bat2.PFReg = strip(string(NanoCon.RunCommand('xxd -p /sys/class/power_supply/sbs-2-0009/reg_block'))); %permenant failure status

end

