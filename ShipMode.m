% Initialization
if ~isdeployed % Stand-alone mode
    if(exist('ArchiveLocation','var')) %Backup old data if ArchiveLocation exists
        save([ArchiveLocation datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '_backup.mat']);
    end
    if length(findall(0)) > 1
        delete(findall(0));
    end
    
    clear
    clc
    fclose('all');
    multiWaitbar('closeAll');
end

opengl software %Was crashing on laptop with hw graphics

iBootIP = readIBootIP'; %Reads /settings/iBootIP.txt to get iBoot IP's
for i = 1:length(iBootIP) %Turn on all iBoot bars prior to matching
    turnOnPower(1:8,iBootIP{i});
end

num_of_devices=inputdlg('Enter number of devices between 1-20');
while str2double(num_of_devices)>20 || str2double(num_of_devices)<1 || isnan(str2double(num_of_devices))
    num_of_devices=inputdlg('Enter number of devices between 1-20');
end

% answer = questdlg('Select to turn ship mode on/off after calibration','Ship Mode','On','Off','On'); %Shipmode prompmt
% switch answer
%     case 'On'
        shipMode = 1;
%     case 'Off'
%         shipMode = 0;
%     case ''
%         shipMode = 0;
% end

StartupTime = now;
ArchiveLocation = [ShowPath '\Archive\'];
if(~exist(ArchiveLocation,'dir')) %Make archive folder
    mkdir(ArchiveLocation)
end

[IP,hostname] = SimpleDiscover3(str2double(num_of_devices)); %Initialization
for j=1:length(IP)
    devices(j).Connection = NanoSSH(IP(j),'arthrex','Arthrex1');
    while ~contains(devices(j).Connection.RunCommand('ls -a /tmp/'),'UPower_DBUS.py')
        UploadScripts(devices(j).Connection);
    end
    Discharged(j) = 0;
    Charged(j) = 0;
    Finished(j) = 0;
    Failure(j) = 0;
    Recharged(j) = 0;
    shipModeFail(j) = 0;
    errorCount(j) = 0;
    devices(j).outlet = 0;
    oldLabel{j}=sprintf('Device %i',j);
    etaBool(j) = 0;
    devices(j).iBootIP = 'unMatched';
    fprintf('Device %s Booted\r\n',hostname(j));
end

devices = iBootMatching(devices,iBootIP,hostname);

for i = 1:length(iBootIP)
    turnOnPower(1:8,iBootIP{i});
end

unMatched = find([devices.outlet] == 0); %Matching Check
if any(unMatched)
    errordlg(sprintf('Following Devices Failed to Match and Will Not Be Calibrated\n%s\nTo Calibrate These Devices Verify Connections and Restart Program',strjoin(hostname(logical(unMatched)),'\n')))
    Finished(logical(unMatched)) = 1;
end

wrongFW = zeros(1,length(devices)); % Firmware Check
for j = find(~Finished)
    out = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/uevent');
    FW(j) = regexp(out,'SERIAL_NUMBER=(?<version>\S+)','names');
    if ~strcmp(FW(j).version,'0004')
        wrongFW(j) = 1;
    end
end
if any(wrongFW)
    errordlg(sprintf('Following Devices Have Wrong Battery Firmware and Will Not Be Calibrated\n%s\nTo Calibrate These Devices Reinstall Battery Firmware and Restart Program',strjoin(hostname(logical(wrongFW)),'\n')))
    Finished(logical(wrongFW)) = 1;
end

i=0;

jj=0; % For numbering the devices in the status bar to make them easy to count
for j = find(~Finished)
    jj=jj+1;
    devNumber(j) = jj;
end
%Main Loop
loopStart = tic;
while ~all(Finished) && toc(loopStart) < 60*60*8 %Eight hour time limit
    i=i+1; %iterate i
    for j=find(~Finished)
        try
            devices(j).Connection.RunCommandAsync('python3 /tmp/UPower_DBUS.py');
        catch err %Remove device from loop if error
            try
                getReport(err)
                errorCount(j) = errorCount(j)+1;
                if errorCount(j) >= 5 % Device failure after 5 errors
                    Finished(j) = 1; %Remove device from loop if error
                    Failure(j) = 1;
                    multiWaitbar(oldLabel{j},'Color','R');
                    newLabel = sprintf('%s Calibration Error',hostname(j));
                    multiWaitbar(oldLabel{j});
                    if ~strcmp(oldLabel{j},newLabel)
                        multiWaitbar(oldLabel{j},'Relabel',newLabel);
                    end
                    oldLabel{j}=newLabel;
                    drawnow
                end
            catch err
                getReport(err)
            end
        end
    end
    wait4Async([devices(~Finished).Connection],60);
    for j=find(~Finished)
        try
            % Reads Power and Controls iBoot and shutdown
            [devices(j).out(i).Bat1,devices(j).out(i).Bat2]=ReadPowerAsync(devices(j).Connection);
            
            
            
            if ~Charged(j) && max([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]) >= 50
                turnOffPower(devices(j).outlet,devices(j).iBootIP);
                Charged(j) = 1;
                devices(j).Connection.WriteRegister("9fff0001",22); %Turns backlight to max to drain battery faster
            elseif Charged(j) && ~Discharged(j) && max([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]) <= 50 
                Discharged(j) = 1;
                turnOnPower(devices(j).outlet,devices(j).iBootIP);
            elseif Charged(j) && Discharged(j)
                pause(1);
                Finished(j) = 1;
                Recharged(j) = 1;
                try
                    if shipMode %put device into ship mode
                        devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-0009/reg_address');
                        devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-000b/reg_address');
                        Bat1Status{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block'); %Registers with ship mode flag
                        Bat2Status{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block');
                        ShipMode_tic = tic;
                        Bat1Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block');
                        Bat2Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block');
                        while ~contains(Bat1Status_ShipMode{j}(36),'1') || ~contains(Bat2Status_ShipMode{j}(36),'1') %Looking at ship mode flags
                            devices(j).Connection.RunCommand('/usr/sbin/battery-ship-mode') %arms ship mode and should flip ship mode flags if successful
                            pause(1)
                            Bat1Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block');
                            Bat2Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block');
                            pause(9)
                            if toc(ShipMode_tic) > 180
                                fprintf('Device %s Failed to go into Ship mode',hostname(j));
                                shipModeFail(j) = 1;
                                break;
                            end
                        end
                    end
                catch
                end
                devices(j).Connection.RunCommand('echo 0x0000 > /sys/class/power_supply/sbs-2-0009/reg_address'); %re-initialize power register to 0x00
                devices(j).Connection.RunCommand('echo 0x0000 > /sys/class/power_supply/sbs-2-000b/reg_address');
                devices(j).Connection.RunCommand('systemctl poweroff'); %power off ccu
                pause(20); %pause to allow full shutdown
                turnOffPower(devices(j).outlet,devices(j).iBootIP); %power off outlet, putting device in ship mode
            end
            
            % Updates statusbar
            chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
            
            if Finished(j) && shipMode
                if shipModeFail(j)
                    status='Finished, Battery Failed To Go Into Ship Mode';
                    multiWaitbar(oldLabel{j},'Color','R');
                    progress = 1;
                else
                    status='Finished, Battery in Ship Mode';
                    multiWaitbar(oldLabel{j},'Color','G');
                    progress = 1;
                end
            elseif Finished(j) && ~shipMode
                status = 'Finished Battery in Default Mode';
                multiWaitbar(oldLabel{j},'Color','G');
                progress = 1;
            elseif Discharged(j)
                status='Battery at 50% placing into ship mode';
                chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = 1;
            elseif Charged(j)
                status='Discharging to around 50%';
                chargePercent = double(max([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = 2*(1-chargePercent);
            else
                status='Charging to around 50%';
                chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = 2*chargePercent;
            end
            
            if (progress > .45) && ~etaBool(j)
                etaTic(j) = tic;
                etaBool(j) = 1;
            end
            if progress <= .5
                eta = round(100 * (1-progress));
            else
                elapsedtime = toc(etaTic(j));
                eta = round(elapsedtime/60 .* (1-progress) ./ (progress - .45));
            end
            newLabel = sprintf('#%-3i %-9s %-46s CurrentCharge=%3.3g%% Total Progress = %3.3g%s ETA = %3i min ReadCount=%i',devNumber(j),hostname(j),status,chargePercent*100,progress*100,'%',eta,i);
            multiWaitbar(oldLabel{j});
            if ~strcmp(oldLabel{j},newLabel)
                multiWaitbar(oldLabel{j},'Relabel',newLabel);
            end
            multiWaitbar(newLabel,progress);
            oldLabel{j}=newLabel;
            drawnow
        catch err %Remove device from loop if error
            try
                getReport(err)
                errorCount(j) = errorCount(j)+1;
                if errorCount(j) >= 5 % Device failure after 5 errors
                    Finished(j) = 1; %Remove device from loop if error
                    Failure(j) = 1;
                    multiWaitbar(oldLabel{j},'Color','R');
                    newLabel = sprintf('%s Connection Error',hostname(j));
                    multiWaitbar(oldLabel{j});
                    if ~strcmp(oldLabel{j},newLabel)
                        multiWaitbar(oldLabel{j},'Relabel',newLabel);
                    end
                    oldLabel{j}=newLabel;
                    drawnow
                end
            catch err
                getReport(err)
            end
        end
    end
end

% save([ShowPath '\Archive\' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.mat']);
% 
fileID = fopen([ShowPath '\Archive\JustShipMode ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'w');%Full Report
fprintf(fileID,'Serial Number,Calibrated,Recharged to 50%%,Pass Capacity,Connection Success,Ship Mode,Bat1 Initial Charge Capacity,Bat1 Final Charge Capacity,Bat2 Initial Charge Capacity,Bat2 Final Charge Capacity\n');
fclose(fileID);
% 
for j = 1:length(devices) %CSV Reports
    try
        Bat1 = [devices(j).out.Bat1];
        Bat2 = [devices(j).out.Bat2];
        Bat1charge_full = [Bat1.charge_full];
        Bat1charge_full_init = Bat1charge_full(1);
        Bat1charge_full_end = Bat1charge_full(end);
        Bat2charge_full = [Bat2.charge_full];
        Bat2charge_full_init = Bat2charge_full(1);
        Bat2charge_full_end = Bat2charge_full(end);
        lowCapacity = Bat1charge_full_end < 2400 || Bat2charge_full_end < 2400;
%         if shipMode
%             shipModeResult = ~shipModeFail(j);
%         else
%             shipModeResult= 0;
%         end
    catch
        Bat1charge_full_init = '';
        Bat1charge_full_end = '';
        Bat2charge_full_init = '';
        Bat2charge_full_end = '';
        shipModeResult = [];
        lowCapacity = [];
    end
%     fileID = fopen([ShowPath '\Archive\' char(hostname(j)) ' ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'w'); %Individual Report
%     fprintf(fileID,'Arthrex California Technology Inc.\nMAF-8.2.4-184-1\nCCU NANO Arthroscopy Console Battery Calibration Form\nRevision Level: 0\n \n');
%     fprintf(fileID,'Serial Number,Calibrated,Recharged to 50%%,Pass Capacity,Connection Success,Ship Mode,Bat1 Initial Charge Capacity,Bat1 Final Charge Capacity,Bat2 Initial Charge Capacity,Bat2 Final Charge Capacity\n');
%     fprintf(fileID,'%s,%s,%s,%s,%s,%s,%i,%i,%i,%i\n',...
%         hostname(j),bool2PassFail(Discharged(j)),bool2PassFail(Recharged(j)),bool2PassFail(~lowCapacity),bool2PassFail(~Failure(j)),bool2OnOff(shipModeResult),...
%         Bat1charge_full_init,Bat1charge_full_end,Bat2charge_full_init,Bat2charge_full_end);
%     fprintf(fileID,'\n\n\n\n-------------------------------\nTechnician Signature and Date');
%     fclose(fileID);
%     
    fileID = fopen([ShowPath '\Archive\JustShipMode ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'a'); %Full Report
    fprintf(fileID,'%s,%s,%s,%s,%s,%s,%i,%i,%i,%i\n',...
        hostname(j),bool2PassFail(Discharged(j)),bool2PassFail(Recharged(j)),bool2PassFail(~lowCapacity),bool2PassFail(~Failure(j)),bool2OnOff(shipModeResult),...
        Bat1charge_full_init,Bat1charge_full_end,Bat2charge_full_init,Bat2charge_full_end);
    fclose(fileID);
%     
end

fileID = fopen([ShowPath '\Archive\JustShipMode ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'a');%Full Report
fclose(fileID);
winopen([ShowPath '\Archive\JustShipMode ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'])%Full Report


function PassFail = bool2PassFail(bool)
if isempty(bool)
    PassFail = '';
elseif bool
    PassFail = 'PASS';
else
    PassFail = 'FAIL';
end
end

function OnOff = bool2OnOff(bool)
if isempty(bool)
    OnOff = '';
elseif bool
    OnOff = 'ON';
else
    OnOff = 'OFF';
end
end



