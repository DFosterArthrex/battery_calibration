% close all
% DirList = dir(fullfile('*.mat'));
% for k = 1:length(DirList)
%     clearvars -except DirList k
%     if ~contains(DirList(k).name,'backup')
%         load(fullfile(DirList(k).name));
        %% Start Time
        a= sprintf('<html><span style="font-size:45px"><br><b>%s</b><br><br></span></html>',datestr(StartupTime));
        disp(a)
        for j = find(Recharged)
            a= sprintf('<html><span style="font-size:22px"><b>%s</b><br><br></span></html>',hostname(j));
            disp(a)
        end
        
        %%
        for j = find(Recharged)
            if ~isempty(devices(j).out)
                %% Cell Voltages
                figure('Position', [0 0 1920 960]);
                a= sprintf('<html><span style="font-size:20px"><b>%s, %s</b><br><br></span></html>', hostname(j), datestr(StartupTime));
                disp(a)
                Bat1 = [devices(j).out.Bat1];
                Bat2 = [devices(j).out.Bat2];
                Bat1charge_full = [Bat1.charge_full];
                Bat2charge_full = [Bat2.charge_full];
                a = sprintf(['<html><span style="font-size:20px">BAT1 Initial Charge Capacity = <b>%g </b> BAT1 Calibrated Charge Capacity = <b>%g </b><br><br>BAT2 Initial Charge Capacity = <b>%g </b> BAT2 Calibrated Charge Capacity = <b>%g </b>' ...
                    '<br></span></html>'], ...
                    Bat1charge_full(1),Bat1charge_full(end),Bat2charge_full(1),Bat2charge_full(end));
                disp(a)
                if isfield(devices(j),'Bat1Balance')
                    balanceString = sprintf('Bat 1 Balance = %i, Bat2 Balance = %i (Balance = Max cell voltage - min cell Voltage sampled immediatly before discharging)',...
                        devices(j).Bat1Balance, devices(j).Bat2Balance);
                    htmlString(balanceString)
                end
                AXES(2-1) = subplot(1,2,2-1);
                plot([Bat1.time], [Bat1.cell_1_voltage], [Bat1.time], [Bat1.cell_2_voltage],[Bat1.time], [Bat1.cell_3_voltage],[Bat1.time], [Bat1.cell_4_voltage])
                yyaxis right
                plot( [Bat1.time], [Bat1.charge_percent])
                legend1 = sprintf('cell 1 voltage max = %.0f mV, min = %.0f mV', max([Bat1.cell_1_voltage]), min([Bat1.cell_1_voltage]));
                legend2 = sprintf('cell 2 voltage max = %.0f mV, min = %.0f mV', max([Bat1.cell_2_voltage]), min([Bat1.cell_2_voltage]));
                legend3 = sprintf('cell 3 voltage max = %.0f mV, min = %.0f mV', max([Bat1.cell_3_voltage]), min([Bat1.cell_3_voltage]));
                legend4 = sprintf('cell 4 voltage max = %.0f mV, min = %.0f mV', max([Bat1.cell_4_voltage]), min([Bat1.cell_4_voltage]));
                htmlString(['Bat1 ' legend1 '; ' legend2 '; ' legend3 '; ' legend4])
                legendPercent = sprintf('Charge Percent');
                legend({legend1,legend2,legend3,legend4,legendPercent},'Location','northeast')%,'NumColumns',2)
                title(sprintf('%s Bat1',hostname(j)));
                yyaxis right
                ylabel('Charge %')
                yyaxis left
                ylabel('Cell Voltage (mV)')
                
                
                AXES(2) = subplot(1,2,2);
                plot([Bat2.time], [Bat2.cell_1_voltage], [Bat2.time], [Bat2.cell_2_voltage],[Bat2.time], [Bat2.cell_3_voltage],[Bat2.time], [Bat2.cell_4_voltage])
                yyaxis right
                plot( [Bat2.time], [Bat1.charge_percent])
                legend1 = sprintf('cell 1 voltage max = %.0f mV, min = %.0f mV', max([Bat2.cell_1_voltage]), min([Bat2.cell_1_voltage]));
                legend2 = sprintf('cell 2 voltage max = %.0f mV, min = %.0f mV', max([Bat2.cell_2_voltage]), min([Bat2.cell_2_voltage]));
                legend3 = sprintf('cell 3 voltage max = %.0f mV, min = %.0f mV', max([Bat2.cell_3_voltage]), min([Bat2.cell_3_voltage]));
                legend4 = sprintf('cell 4 voltage max = %.0f mV, min = %.0f mV', max([Bat2.cell_4_voltage]), min([Bat2.cell_4_voltage]));
                htmlString(['Bat2 ' legend1 '; ' legend2 '; ' legend3 '; ' legend4])
                legendPercent = sprintf('Charge Percent');
                legend({legend1,legend2,legend3,legend4,legendPercent},'Location','northeast')%,'NumColumns',2)
                title(sprintf('%s Bat2',hostname(j)));
                yyaxis right
                ylabel('Charge %')
                yyaxis left
                ylabel('Cell Voltage (mV)')
                
                set(AXES, 'YLim', [3300 4300]);
                %                 allXLim = get(AXES, {'XLim'});
                %                 allXLim = cat(2, allXLim{:});
                %
                
            end
            close all
        end
%     end
% end

close all

function htmlString(s)
a= sprintf('<html><span style="font-size:16px"><br>%s<br></span></html>',s);
disp(a)
end