if(~exist([ShowPath,'\settings\'],'dir'))
    mkdir([ShowPath,'\settings\'])
end

IPAddress = [];
if(exist([ShowPath,'\settings\iBootIP.txt'],'file'))
    try
    outlets = importdata([ShowPath,'\settings\iBootIP.txt']);
    
    %field names on first line
    tmp = textscan(outlets{1},'%s');
    header = tmp{1};
    numFields = length(header);
    expresion = [sprintf('(?<%s>\\S*)\\s*',header{1:end-1}) sprintf('!(?<%s>(\\S*\\s*)*)',header{end})];
    IPAddress = [];
    %Print Outlet Info
    for i = 2:length(outlets)
        DevicesTmp = regexp(outlets{i},expresion,'names');
        Fields = fieldnames(DevicesTmp);
        for j = 1:length(Fields)
            Devices(i-1).(Fields{j}) = DevicesTmp.(Fields{j});
        end
        IPAddress = [IPAddress; Devices(i-1).IPAddress(1:end)];
        %     LogText(handles,sprintf('Device #%d',i-1))
        %     for j = 1:numFields
        %         LogText(handles,sprintf(' %15s: %-15s',header{j},handles.Devices(i-1).(header{j})))
        %     end
    end
    numDevices = cellstr(int2str(length(Devices)));
    IPAddress = cellstr(IPAddress);
    catch
    end
end
    
if ~isempty(IPAddress)
    answer = inputdlg('Enter Number of iBootBars','Enter Number of iBootBars',[1 35],numDevices);
    numDevices = str2double(answer);
    prompt = [];
    for i = 1:numDevices
        prompt = [prompt;sprintf('iBootBar %i IPAddress',i)];
    end
    prompt = cellstr(prompt);
    if numDevices ~= length(Devices)
        IP = inputdlg(prompt,'Enter Device IPAddresses',[1 35]);
    else
        IP = inputdlg(prompt,'Enter Device IPAddresses',[1 35],IPAddress);
    end
    fid = fopen([ShowPath,'\settings\iBootIP.txt'],'w');
    fprintf(fid,'%-20s %-20s %-s\r\n',"Type", "IPAddress", "Notes");
    for i=1:numDevices
        NoteStr = sprintf('!Device %i Comments Here',i);
        fprintf(fid,'%-20s %-20s %-s\r\n',"iBoot", IP{i}, NoteStr);
    end
    fclose(fid);
    
else
    answer = inputdlg('Enter Number of iBootBars','Enter Number of iBootBars',[1 35],"3");
    numDevices = str2double(answer);
    prompt = [];
    for i = 1:numDevices
        prompt = [prompt;sprintf('iBootBar %i IPAddress',i)];
    end
    prompt = cellstr(prompt);
    [defIn{1:numDevices}] = deal("192.168.0.");
    IP = inputdlg(prompt,'Enter Device IPAddresses',[1 35],cellstr(defIn));
    fid = fopen([ShowPath,'\settings\iBootIP.txt'],'w');
    fprintf(fid,'%-20s %-20s %-s\r\n',"Type", "IPAddress", "Notes");
    for i=1:numDevices
        NoteStr = sprintf('!Device %i Comments Here',i);
        fprintf(fid,'%-20s %-20s %-s\r\n',"iBoot", IP{i}, NoteStr);
    end
    fclose(fid);
end