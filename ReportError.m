function [errorText,errorTotal] = ReportError(err,varargin)
%prints varargin name and value so you can print i or cyclenumber in
%error.txt

try
    errorText = getReport(err);
    fid = fopen([pwd '\error.txt']);
    A = fread(fid,'*char')';
    out = regexp(A,'Error Total: (\d+)','match');
    errorTotal=str2double(out{end})+1;
    fclose(fid);
    
    fid = fopen([pwd '\error.txt'],'a');
    fprintf(fid,'\r\n%s\r\n',datestr(now,'yy-mm-dd_HH.MM.SS.FFF'));
    for k = 1:nargin-1
        try
            fprintf(fid,'%s = %i\r\n',inputname(k+1),varargin{k});
        catch varargError
            getReport(varargError);
        end
    end
    if(iscell(errorText))
        fprintf(fid,'%s',errorText{:});
    else
        fprintf(fid,'%s',errorText);
    end
    fprintf(fid,'\r\nError Total: %d',errorTotal);
    fclose(fid);

catch 
    errorText = getReport(err);
end
