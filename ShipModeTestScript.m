turnOnPower(1:8)
StartupTime = now;
ArchiveLocation = [pwd '\ShipModeTestArchive\' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '\'];
if(~exist(ArchiveLocation,'dir'))
    mkdir(ArchiveLocation)
end
SimpleDiscover;
[IP,hostname] = findIPAddress2;
for j=1:length(IP)
    devices(j).Connection = NanoSSH(IP(j),'arthrex','Arthrex1');
    devices(j).Connection.RunCommand('/usr/sbin/battery-ship-mode')
end
pause(10);
for j=1:length(IP)
    devices(j).Connection.RunCommand('systemctl poweroff');
end
pause(20);
turnOffPower(1:8)
uiwait(msgbox('Turn On All Devices Wait For All To Boot Then Press OK'))
for j=1:length(IP)
    DownloadBootlog(devices(j).Connection,sprintf('%s Device %s.txt', ArchiveLocation, hostname(j)));
    fid = fopen(sprintf('%s Device %s.txt', ArchiveLocation, hostname(j)));
    tempA = fread(fid,'*char')';
    fclose(fid);
    Result(j) = contains(tempA,'attempting to exit ship mode');
    passFail ={'FAIL' 'PASS'};
    fprintf('Result Device %s: %s \r\n',hostname(j),passFail{Result(j)+1});
    fid = fopen(sprintf('%sResult.txt', ArchiveLocation),'a');
    fprintf(fid,'Result Device %s: %s \r\n',hostname(j),passFail{Result(j)+1});
    fclose(fid);
end

function turnOnPower(k)
outlets = zeros(1,8);
outlets(k) = 1;
iBootControl('192.168.0.11',outlets,'ON');
end

function turnOffPower(k)
outlets = zeros(1,8);
outlets(k) = 1;
iBootControl('192.168.0.11',outlets,'OFF');
end