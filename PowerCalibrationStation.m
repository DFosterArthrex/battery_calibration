% Initialization
if ~isdeployed % Stand-alone mode
    if(exist('ArchiveLocation','var')) %Backup old data if ArchiveLocation exists
        save([ArchiveLocation datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '_backup.mat']);
    end
    if length(findall(0)) > 1
        delete(findall(0));
    end
    
    clear
    clc
    fclose('all');
    multiWaitbar('closeAll');
end

opengl software %Was crashing on laptop with hw graphics

iBootIP = readIBootIP'; %Reads /settings/iBootIP.txt to get iBoot IP's
for i = 1:length(iBootIP) %Turn on all iBoot bars prior to matching
    turnOnPower(1:8,iBootIP{i});
end

num_of_devices=inputdlg('Enter number of devices between 1-20');
while str2double(num_of_devices)>20 || str2double(num_of_devices)<1 || isnan(str2double(num_of_devices))
    num_of_devices=inputdlg('Enter number of devices between 1-20');
end

answer = questdlg('Select to turn ship mode on/off after calibration','Ship Mode','On','Off','On'); %Shipmode prompmt
switch answer
    case 'On'
        shipMode = 1;
    case 'Off'
        shipMode = 0;
    case ''
        shipMode = 0;
end

StartupTime = now;
ArchiveLocation = [ShowPath '\Archive\'];
if(~exist(ArchiveLocation,'dir')) %Make archive folder
    mkdir(ArchiveLocation)
end

[IP,hostname] = SimpleDiscover3(str2double(num_of_devices)); %Initialization
for j=1:length(IP)
    devices(j).Connection = NanoSSH(IP(j),'arthrex','Arthrex1');
    while ~contains(devices(j).Connection.RunCommand('ls -a /tmp/'),'UPower_DBUS.py')
        UploadScripts(devices(j).Connection);
    end
    Discharged(j) = 0;
    Charged(j) = 0;
    Finished(j) = 0;
    Failure(j) = 0;
    Recharged(j) = 0;
    shipModeFail(j) = 0;
    errorCount(j) = 0;
    devices(j).outlet = 0;
    oldLabel{j}=sprintf('Device %i',j);
    etaBool(j) = 0;
    devices(j).iBootIP = 'unMatched';
    fprintf('Device %s Booted\r\n',hostname(j));
    chargedTic(j) = uint64(0);
end

devices = iBootMatching(devices,iBootIP,hostname);

for i = 1:length(iBootIP)
    turnOnPower(1:8,iBootIP{i});
end

unMatched = find([devices.outlet] == 0); %Matching Check
if any(unMatched)
    errordlg(sprintf('Following Devices Failed to Match and Will Not Be Calibrated\n%s\nTo Calibrate These Devices Verify Connections and Restart Program',strjoin(hostname(logical(unMatched)),'\n')))
    Finished(logical(unMatched)) = 1;
end
for j = find(~Finished)
    devices(j).Connection.RunCommand('echo test')
    devices(j).Connection.RunCommandAsync('battery-program-firmware bq78350-df-v4.bin');
end

wait4Async([devices(~Finished).Connection],60);
pause(5)
installErrors = zeros(size(Finished));
installed = false(size(Finished));
for j = find(~Finished)
    while ~installed(j)
        chargeFullBat1 = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-000b/charge_full');
        chargeFullBat2 = devices(j).Connection.RunCommand('cat /sys/class/power_supply/sbs-2-0009/charge_full');
        installed(j) = contains(chargeFullBat1,'2500') && contains(chargeFullBat2,'2500');
        if installErrors(j) < 3 && ~installed(j)
            devices(j).Connection.RunCommand('battery-program-firmware bq78350-df-v4.bin');
            installErrors(j) = installErrors(j) + 1;
        elseif installErrors(j) >= 3
            Finished(j) = 1;
            errordlg(sprintf('%s Failed to Program Battery Firmware battery-program-firmware bq78350-df-v4.bin and Will Not Be Calibrated',hostname(j)))
            continue
        end
    end
end


for j = find(~Finished)
    
% 1. reinitialize battery monitor data flash before calibration starts (battery-program-firmware bq78350-df-v4.bin)
% 2. enable battery monitor "lifetime data collection" feature (write 2-byte 0x0023 to register 0x23)
% 3. enable battery monitor "permanent failure" feature (write 2-byte 0x0024 to register 0x24)
% 4. enable battery monitor "blackbox recorder" feature (write 2-byte 0x0025 to register 0x25)
% 5. enable permanent failure monitoring for cell imbalance, overvoltage, and undervoltage (write 1-byte 0x23 to register 0x451D)
% 6. verify that the "manufacturing status" register has the correct settings (2-byte register 0x57 == 0x00F0)
    
    devices(j).Connection.RunCommand('echo 0x23 > /sys/class/power_supply/sbs-2-000b/mfr_cmd');
    devices(j).Connection.RunCommand('echo 0x23 > /sys/class/power_supply/sbs-2-0009/mfr_cmd');
    devices(j).Connection.RunCommand('echo 0x24 > /sys/class/power_supply/sbs-2-000b/mfr_cmd');
    devices(j).Connection.RunCommand('echo 0x24 > /sys/class/power_supply/sbs-2-0009/mfr_cmd');
    devices(j).Connection.RunCommand('echo 0x25 > /sys/class/power_supply/sbs-2-000b/mfr_cmd');
    devices(j).Connection.RunCommand('echo 0x25 > /sys/class/power_supply/sbs-2-0009/mfr_cmd');
    
    devices(j).Connection.RunCommand('echo 0x451D > /sys/class/power_supply/sbs-2-000b/reg_address');
    devices(j).Connection.RunCommand('echo 0x451D > /sys/class/power_supply/sbs-2-0009/reg_address');
    devices(j).Connection.RunCommand('echo 1 > /sys/class/power_supply/sbs-2-000b/reg_size');
    devices(j).Connection.RunCommand('echo 1 > /sys/class/power_supply/sbs-2-0009/reg_size');
    devices(j).Connection.RunCommand('echo 0x3 > /sys/class/power_supply/sbs-2-000b/reg_data');
    devices(j).Connection.RunCommand('echo 0x3 > /sys/class/power_supply/sbs-2-0009/reg_data');
    devices(j).Bat1Reg451D = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-000b/reg_block');
    devices(j).Bat2Reg451D = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-0009/reg_block');

    
    devices(j).Connection.RunCommand('echo 0x57 > /sys/class/power_supply/sbs-2-000b/reg_address');
    devices(j).Connection.RunCommand('echo 0x57 > /sys/class/power_supply/sbs-2-0009/reg_address');
    devices(j).Bat1Reg57 = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-000b/reg_block');
    devices(j).Bat2Reg57 = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-0009/reg_block');
    try
        assert(contains(devices(j).Bat1Reg451D,'0000000 0003','IgnoreCase',true));
        assert(contains(devices(j).Bat2Reg451D,'0000000 0003','IgnoreCase',true));
        assert(contains(devices(j).Bat1Reg57,'00F0','IgnoreCase',true));
        assert(contains(devices(j).Bat2Reg57,'00F0','IgnoreCase',true));
    catch
        devices(j).Connection.RunCommand('echo 0x451D > /sys/class/power_supply/sbs-2-000b/reg_address');
        devices(j).Connection.RunCommand('echo 0x451D > /sys/class/power_supply/sbs-2-0009/reg_address');
        devices(j).Bat1Reg451D = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-000b/reg_block');
        devices(j).Bat2Reg451D = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-0009/reg_block');
        devices(j).Connection.RunCommand('echo 0x57 > /sys/class/power_supply/sbs-2-000b/reg_address');
        devices(j).Connection.RunCommand('echo 0x57 > /sys/class/power_supply/sbs-2-0009/reg_address');
        devices(j).Bat1Reg57 = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-000b/reg_block');
        devices(j).Bat2Reg57 = devices(j).Connection.RunCommand('hexdump /sys/class/power_supply/sbs-2-0009/reg_block');
        assert(contains(devices(j).Bat1Reg451D,'0000000 0003','IgnoreCase',true));
        assert(contains(devices(j).Bat2Reg451D,'0000000 0003','IgnoreCase',true));
        assert(contains(devices(j).Bat1Reg57,'00F0','IgnoreCase',true));
        assert(contains(devices(j).Bat2Reg57,'00F0','IgnoreCase',true));
    end
end

i=0;

jj=0; % For numbering the devices in the status bar to make them easy to count
for j = find(~Finished)
    jj=jj+1;
    devNumber(j) = jj;
end
%Main Loop
loopStart = tic;
while ~all(Finished) && toc(loopStart) < 60*60*8 %Eight hour time limit
    i=i+1; %iterate i
    for j=find(~Finished)
        try
            devices(j).Connection.RunCommandAsync('python3 /tmp/UPower_DBUS.py');
        catch err %Remove device from loop if error
            try
                getReport(err)
                errorCount(j) = errorCount(j)+1;
                if errorCount(j) >= 5 % Device failure after 5 errors
                    Finished(j) = 1; %Remove device from loop if error
                    Failure(j) = 1;
                    multiWaitbar(oldLabel{j},'Color','R');
                    newLabel = sprintf('%s Calibration Error',hostname(j));
                    multiWaitbar(oldLabel{j});
                    if ~strcmp(oldLabel{j},newLabel)
                        multiWaitbar(oldLabel{j},'Relabel',newLabel);
                    end
                    oldLabel{j}=newLabel;
                    drawnow
                end
            catch err
                getReport(err)
            end
        end
    end
    wait4Async([devices(~Finished).Connection],60);
    for j=find(~Finished)
        try
            % Reads Power and Controls iBoot and shutdown
            [devices(j).out(i).Bat1,devices(j).out(i).Bat2]=ReadPowerAsync(devices(j).Connection);
            assert(strcmp(devices(j).out(i).Bat1.PFReg,"00000000") && strcmp(devices(j).out(i).Bat2.PFReg,"00000000"),'PF Reg: %s %s', devices(j).out(i).Bat1.PFReg,devices(j).out(i).Bat2.PFReg)
            if ~Charged(j) && devices(j).out(i).Bat1.charge_now >= devices(j).out(i).Bat1.charge_full - 200 && devices(j).out(i).Bat2.charge_now >= devices(j).out(i).Bat2.charge_full - 200 && ...
                    devices(j).out(i).Bat1.current_now <= 50 && devices(j).out(i).Bat2.current_now <= 50%If both batteries near full then turn off power and move to testing next condition
                if chargedTic(j) == 0
                    chargedTic(j) = tic;
                elseif toc(chargedTic(j)) > 120
                    devices(j).Bat1Balance = voltRange(devices(j).out(i).Bat1);
                    devices(j).Bat2Balance = voltRange(devices(j).out(i).Bat2);
                    turnOffPower(devices(j).outlet,devices(j).iBootIP);
                    Charged(j) = 1;
                    devices(j).Connection.WriteRegister("9fff0001",22); %Turns backlight to max to drain battery faster
                end
            elseif Charged(j) && ~Discharged(j) && strlength(devices(j).out(i).Bat1.Reg56) >= 21 && strlength(devices(j).out(i).Bat2.Reg56) >= 21 %Register a flag for if battery drops below voltage threshold necesary for qualified discharge
                qualDischarge1 = cellstr(devices(j).out(i).Bat1.Reg56);
                qualDischarge1 = str2double(qualDischarge1{1}(21)); %Pulling flag out of register into a boolean
                qualDischarge2 = cellstr(devices(j).out(i).Bat2.Reg56);
                qualDischarge2 = str2double(qualDischarge2{1}(21)); %Pulling flag out of register into a boolean
                if qualDischarge1 && qualDischarge2 %If both batteries have undergone a qualified discharge than turn on power and move to testing next condition
                    turnOnPower(devices(j).outlet,devices(j).iBootIP);
                    Discharged(j) = 1;
                end
            elseif Charged(j) && Discharged(j) && devices(j).out(i).Bat1.charge_percent >= 50 && devices(j).out(i).Bat2.charge_percent >= 50%Charge both batteries to 50% then finish up
                Finished(j) = 1;
                Recharged(j) = 1;
                try
                    if shipMode %put device into ship mode
                        devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-000b/reg_address');
                        devices(j).Connection.RunCommand('echo 0x54 > /sys/class/power_supply/sbs-2-0009/reg_address');
                        Bat1Status{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block'); %Registers with ship mode flag
                        Bat2Status{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block');
                        ShipMode_tic = tic;
                        Bat1Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block');
                        Bat2Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block');
                        while ~contains(Bat1Status_ShipMode{j}(36),'1') || ~contains(Bat2Status_ShipMode{j}(36),'1') %Looking at ship mode flags
                            devices(j).Connection.RunCommand('/usr/sbin/battery-ship-mode') %arms ship mode and should flip ship mode flags if successful
                            pause(1)
                            Bat1Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-000b/reg_block');
                            Bat2Status_ShipMode{j} = devices(j).Connection.RunCommand('xxd -b /sys/class/power_supply/sbs-2-0009/reg_block');
                            pause(9)
                            if toc(ShipMode_tic) > 180
                                fprintf('Device %s Failed to go into Ship mode',hostname(j));
                                shipModeFail(j) = 1;
                                break;
                            end
                        end
                    end
                catch
                end
                devices(j).Connection.RunCommand('echo 0x0000 > /sys/class/power_supply/sbs-2-000b/reg_address'); %re-initialize power register to 0x00
                devices(j).Connection.RunCommand('echo 0x0000 > /sys/class/power_supply/sbs-2-0009/reg_address');
                devices(j).Connection.RunCommand('systemctl poweroff'); %power off ccu
                pause(20); %pause to allow full shutdown
                turnOffPower(devices(j).outlet,devices(j).iBootIP); %power off outlet, putting device in ship mode
            end
            
            % Updates statusbar
            chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
            
            if Finished(j) && shipMode
                if shipModeFail(j)
                    status='Finished, Battery Failed To Go Into Ship Mode';
                    multiWaitbar(oldLabel{j},'Color','R');
                    progress = 1;
                else
                    status='Finished, Battery in Ship Mode';
                    multiWaitbar(oldLabel{j},'Color','G');
                    progress = 1;
                end
            elseif Finished(j) && ~shipMode
                status = 'Finished Battery in Default Mode';
                multiWaitbar(oldLabel{j},'Color','G');
                progress = 1;
            elseif Discharged(j)
                status='Charging to around 50%';
                chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = min([(.8 + ((1-.8)/(.5-.07))*(chargePercent-.05) ),.99]);
            elseif Charged(j)
                status='Discharging to around 7%';
                chargePercent = double(max([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = .4 + (chargePercent-1)*(.8-0.4)/(.07-1);
            else
                status='Charging to Near Full';
                chargePercent = double(min([devices(j).out(i).Bat1.charge_percent,devices(j).out(i).Bat2.charge_percent]))/100;
                progress = (.4/(1-0))*(chargePercent-0);
            end
            
            if (progress > .45) && ~etaBool(j)
                etaTic(j) = tic;
                etaBool(j) = 1;
            end
            if progress <= .5
                eta = round(300 * (1-progress));
            else
                elapsedtime = toc(etaTic(j));
                eta = round(elapsedtime/60 .* (1-progress) ./ (progress - .45));
            end
            newLabel = sprintf('#%-3i %-9s %-46s CurrentCharge=%3.3g%% Total Progress = %3.3g%s ETA = %3i min ReadCount=%i',devNumber(j),hostname(j),status,chargePercent*100,progress*100,'%',eta,i);
            multiWaitbar(oldLabel{j});
            if ~strcmp(oldLabel{j},newLabel)
                multiWaitbar(oldLabel{j},'Relabel',newLabel);
            end
            multiWaitbar(newLabel,progress);
            oldLabel{j}=newLabel;
            drawnow
        catch err %Remove device from loop if error
            try
                getReport(err)
                errorCount(j) = errorCount(j)+1;
                if errorCount(j) >= 5 % Device failure after 5 errors
                    Finished(j) = 1; %Remove device from loop if error
                    Failure(j) = 1;
                    multiWaitbar(oldLabel{j},'Color','R');
                    newLabel = sprintf('%s Connection Error',hostname(j));
                    multiWaitbar(oldLabel{j});
                    if ~strcmp(oldLabel{j},newLabel)
                        multiWaitbar(oldLabel{j},'Relabel',newLabel);
                    end
                    oldLabel{j}=newLabel;
                    drawnow
                end
            catch err
                getReport(err)
            end
        end
    end
end

save([ShowPath '\Archive\' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.mat']);

fileID = fopen([ShowPath '\Archive\FullTestReport ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'w');%Full Report
fprintf(fileID,['Serial Number,Calibrated,Recharged to 50%%,'...
'Pass Capacity,Pass Balance,Connection Success,Ship Mode,'...
'Bat1 Initial Charge Capacity,Bat1 Final Charge Capacity,'...
'Bat2 Initial Charge Capacity,Bat2 Final Charge Capacity,'...
'Bat1 Balance,Bat2 Balance\n']);
fclose(fileID);

for j = 1:length(devices) %CSV Reports
    try
        Bat1 = [devices(j).out.Bat1];
        Bat2 = [devices(j).out.Bat2];
        Bat1charge_full = [Bat1.charge_full];
        Bat1charge_full_init = Bat1charge_full(1);
        Bat1charge_full_end = Bat1charge_full(end);
        Bat2charge_full = [Bat2.charge_full];
        Bat2charge_full_init = Bat2charge_full(1);
        Bat2charge_full_end = Bat2charge_full(end);
        lowCapacity = Bat1charge_full_end < 2400 || Bat2charge_full_end < 2400;
        BatBalanceBool = devices(j).Bat1Balance <= 40 && devices(j).Bat2Balance <= 40;
        if shipMode
            shipModeResult = ~shipModeFail(j);
        else
            shipModeResult= 0;
        end
    catch
        Bat1charge_full_init = '';
        Bat1charge_full_end = '';
        Bat2charge_full_init = '';
        Bat2charge_full_end = '';
        shipModeResult = [];
        lowCapacity = [];
        BatBalanceBool = '';
    end
    fileID = fopen([ShowPath '\Archive\' char(hostname(j)) ' ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'w'); %Individual Report
    fprintf(fileID,'Arthrex California Technology Inc.\nMAF-8.2.4-184-1\nCCU NANO Arthroscopy Console Battery Calibration Form\nRevision Level: 0\n \n');
    fprintf(fileID,'Serial Number,Calibrated,Recharged to 50%%,Pass Capacity, Pass Balance, Connection Success,Ship Mode,Bat1 Initial Charge Capacity,Bat1 Final Charge Capacity,Bat2 Initial Charge Capacity,Bat2 Final Charge Capacity,Bat1 Balance,Bat2 Balance\n');
    fprintf(fileID,'%s,%s,%s,%s,%s,%s,%s,%i,%i,%i,%i,%i,%i\n',...
        hostname(j),bool2PassFail(Discharged(j)),bool2PassFail(Recharged(j)),bool2PassFail(~lowCapacity),bool2PassFail(BatBalanceBool),bool2PassFail(~Failure(j)),bool2OnOff(shipModeResult),...
        Bat1charge_full_init,Bat1charge_full_end,Bat2charge_full_init,Bat2charge_full_end,devices(j).Bat1Balance,devices(j).Bat2Balance);
    fprintf(fileID,'\n\n\n\n-------------------------------\nTechnician Signature and Date');
    fclose(fileID);
    
    fileID = fopen([ShowPath '\Archive\FullTestReport ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'a'); %Full Report
    fprintf(fileID,'%s,%s,%s,%s,%s,%s,%s,%i,%i,%i,%i,%i,%i\n',...
        hostname(j),bool2PassFail(Discharged(j)),bool2PassFail(Recharged(j)),bool2PassFail(~lowCapacity),bool2PassFail(BatBalanceBool),bool2PassFail(~Failure(j)),bool2OnOff(shipModeResult),...
        Bat1charge_full_init,Bat1charge_full_end,Bat2charge_full_init,Bat2charge_full_end,devices(j).Bat1Balance,devices(j).Bat2Balance);
    fclose(fileID);
    
end

fileID = fopen([ShowPath '\Archive\FullTestReport ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'] ,'a');%Full Report
fclose(fileID);
winopen([ShowPath '\Archive\FullTestReport ' datestr(StartupTime,'yy-mm-dd_HH.MM.SS.FFF') '.csv'])%Full Report


function PassFail = bool2PassFail(bool)
if isempty(bool)
    PassFail = '';
elseif bool
    PassFail = 'PASS';
else
    PassFail = 'FAIL';
end
end

function OnOff = bool2OnOff(bool)
if isempty(bool)
    OnOff = '';
elseif bool
    OnOff = 'ON';
else
    OnOff = 'OFF';
end
end

function maxRange = cellBalance(bat)
cellVoltages(1,:) = filloutliers(double([bat.cell_1_voltage]),'linear','movmedian',100);
cellVoltages(2,:) = filloutliers(double([bat.cell_2_voltage]),'linear','movmedian',100);
cellVoltages(3,:) = filloutliers(double([bat.cell_3_voltage]),'linear','movmedian',100);
cellVoltages(4,:) = filloutliers(double([bat.cell_4_voltage]),'linear','movmedian',100);
maxRange = max(max(cellVoltages) - min(cellVoltages));
end

function range = voltRange(bat)
voltages = [bat.cell_1_voltage bat.cell_2_voltage bat.cell_3_voltage bat.cell_4_voltage];
range = max(voltages) - min(voltages);
end
