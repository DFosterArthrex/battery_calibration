%Returns [IP hostname]
%Pings ips 192.168.0.1-10 then get hostnames of connected devices

function [IP, serialNum] = findIPAddress2()

[~,ArpMsg] = system('arp -a');

k=zeros(size(1:10));
for i = 1:20
    k(i) = contains(ArpMsg,sprintf('192.168.0.%d ',i));
end

lastDigits=find(k);
IP=strings(size(lastDigits));
serialNum=strings(size(lastDigits));
for i=1:length(lastDigits)
    NanoCon=NanoSSH(sprintf('192.168.0.%d',lastDigits(i)),'arthrex','Arthrex1');
    IP(i)= sprintf('192.168.0.%d',lastDigits(i));
    hostname=NanoCon.RunCommand('hostname');
    a=char(strtrim(hostname));    
    dashIndx=regexp(hostname,'-');
    serialNum(i)=a(dashIndx+1:end);
end
