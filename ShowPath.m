function [thePath] = ShowPath() 
  % Show EXE path: 
  if isdeployed % Stand-alone mode. 
    [status, result] = system('set PATH'); 
    thePath = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once')); 
  else % Running from MATLAB. 
    thePath=pwd; 
  end 
return;