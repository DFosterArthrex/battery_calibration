close all
%% Start Time
a= sprintf('<html><span style="font-size:45px"><br><b>%s</b><br><br></span></html>',datestr(StartupTime));
disp(a)
%% Results
if Failure(j)
    status='<b><font color="red">Calibration Failure due to lost connection</b></font>, <b><font color="red">Failed To Go Into Ship Mode</b></font>';
else
    Bat1 = [devices(j).out(end).Bat1];
    Bat2 = [devices(j).out(end).Bat2];
    Bat1charge_full = [Bat1.charge_full];
    Bat2charge_full = [Bat2.charge_full];
    if Bat1charge_full < 2000 || Bat2charge_full < 2000
        if Finished(j) && shipMode
            if shipModeFail(j)
                status='<b><font color="red">Warning Low Capacity</b></font>, <b><font color="red">Failed To Go Into Ship Mode</b></font>';
                progress = 1;
            else
                status='<b><font color="red">Warning Low Capacity</b></font>, <b><font color="green">Ship Mode</b></font>';
                progress = 1;
            end
        elseif Finished(j) && ~shipMode
            status = '<b><font color="red">Warning Low Capacity</b></font> <b><font color="green">Default Mode</b></font>';
            progress = 1;
        end
    elseif Finished(j) && shipMode
        if shipModeFail(j)
            status='<b><font color="green">Calibrated</b></font>, <b><font color="red">Failed To Go Into Ship Mode</b></font>';
            progress = 1;
        else
            status='<b><font color="green">Calibrated</b></font>, <b><font color="green">Ship Mode</b></font>';
            progress = 1;
        end
    elseif Finished(j) && ~shipMode
        status = '<b><font color="green">Calibrated</b></font> <b><font color="green">Default Mode</b></font>';
        progress = 1;
    end
end

a = sprintf(['<html><span style="font-size:45px">'...
    '<br><b>%s</b> %s <br><br>' ...
    '</span></html>'], ...
    hostname(j),status);
disp(a)

%% Charge Capacities
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
Bat1charge_full = [Bat1.charge_full];
Bat2charge_full = [Bat2.charge_full];
a = sprintf(['<html><span style="font-size:20px">'...
    '<br><b>%s</b><br><br>' ...
    'BAT1 Initial Charge Capacity = <b>%g</b><br><br> BAT1 Calibrated Charge Capacity = <b>%g</b><br><br>BAT2 Initial Charge Capacity = <b>%g</b><br><br> BAT2 Calibrated Charge Capacity = <b>%g</b>' ...
    '<br><br></span></html>'], ...
    hostname(j),Bat1charge_full(1),Bat1charge_full(end),Bat2charge_full(1),Bat2charge_full(end));
disp(a)

%% Cell Voltages
figure('Position', [2000 2000 1280 400]);
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(2-1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.cell_1_voltage], [Bat1.time], [Bat1.cell_2_voltage],[Bat1.time], [Bat1.cell_3_voltage],[Bat1.time], [Bat1.cell_4_voltage])
legend({'cell 1 voltage','cell 2 voltage','cell 3 voltage','cell 4 voltage'},'Location','southwest','NumColumns',2)
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.cell_1_voltage], [Bat2.time], [Bat2.cell_2_voltage],[Bat2.time], [Bat2.cell_3_voltage],[Bat2.time], [Bat2.cell_4_voltage])
legend({'cell 1 voltage','cell 2 voltage','cell 3 voltage','cell 4 voltage'},'Location','southwest','NumColumns',2)
title(sprintf('%s Bat2',hostname(j)));


allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
[TF,L,U,C] = isoutlier(allYLim,'gesd','ThresholdFactor',.7);
set(AXES, 'YLim', [max(L,min(allYLim(~TF))) min(U,max(allYLim(~TF)))]);

allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);

%% Charge Percent
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(2-1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.charge_percent])
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.charge_percent])
title(sprintf('%s Bat2',hostname(j)));

allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
[TF,L,U,C] = isoutlier(allYLim,'gesd','ThresholdFactor',.7);
set(AXES, 'YLim', [max(L,min(allYLim(~TF))) min(U,max(allYLim(~TF)))]);

allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);

%% Charge
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(2-1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.charge_now])
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.charge_now])
title(sprintf('%s Bat2',hostname(j)));

allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
[TF,L,U,C] = isoutlier(allYLim,'gesd','ThresholdFactor',.7);
set(AXES, 'YLim', [max(L,min(allYLim(~TF))) min(U,max(allYLim(~TF)))]);

allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);

%% Charge Full
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(2-1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.charge_full])
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.charge_full])
title(sprintf('%s Bat2',hostname(j)));

allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
set(AXES, 'YLim', [min(allYLim), max(allYLim)]);


allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);

%% Temperature
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(2-1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.temp])
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.temp])
title(sprintf('%s Bat2',hostname(j)));

allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
[TF,L,U,C] = isoutlier(allYLim,'gesd','ThresholdFactor',.7);
set(AXES, 'YLim', [max(L,min(allYLim(~TF))) min(U,max(allYLim(~TF)))]);

allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);

%% Current
Bat1 = [devices(j).out.Bat1];
Bat2 = [devices(j).out.Bat2];
AXES(1) = subplot(1,2,2-1);
plot([Bat1.time], [Bat1.current_now])
title(sprintf('%s Bat1',hostname(j)));

AXES(2) = subplot(1,2,2);
plot([Bat2.time], [Bat2.current_now])
title(sprintf('%s Bat2',hostname(j)));

allYLim = get(AXES, {'YLim'});
allYLim = cat(2, allYLim{:});
[TF,L,U,C] = isoutlier(allYLim,'gesd','ThresholdFactor',.7);
set(AXES, 'YLim', [max(L,min(allYLim(~TF))) min(U,max(allYLim(~TF)))]);

allXLim = get(AXES, {'XLim'});
allXLim = cat(2, allXLim{:});
set(AXES, 'XLim', [min(allXLim), max(allXLim)]);
