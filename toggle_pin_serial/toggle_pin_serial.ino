int toggle_pin = 4;

void setup(){
  Serial.begin(9600);

  //Set all the pins we need to output pins
  pinMode(toggle_pin, OUTPUT);
  digitalWrite(toggle_pin, HIGH);
}

void loop (){

  if (Serial.available()) {

    //read serial as a character
    char ser = Serial.read();
    togglePin(toggle_pin, (int)ser);
  }
}

void togglePin(int pin, int delta){
  digitalWrite(pin, LOW);
  delay(delta);
  digitalWrite(pin, HIGH);
}
