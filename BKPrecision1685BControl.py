import serial
import time

#BK Precision 1685B Control
#GETD = GetGet display voltage,current, and status reading from power supply
#SOUT{<output>}[CR] = Output On/Off control 0 = ON 1 = OFF
#VOLT{<voltage>}[CR] = Set voltage level <voltage> = 000-XXX

def toggle_powersupply(COM_Port,state):
	ser = serial.Serial(
	    port=COM_Port,
	    baudrate=9600,
	)
	ser.close()         # close port
	time.sleep(1)
	out = ''
	retry = 0
	if(ser.isOpen() == False):
	    ser.open()
	    time.sleep(2)

	if state == 'on' or state == 'ON':
		while "OK" not in str(out):
			ser.write(str('SOUT0\r\n').encode('UTF-8'))      # Port x ON	
			time.sleep(1)
			out = ser.read(ser.inWaiting())
			# print("retry counter =" + str(retry))
			retry += 1
			if retry > 5:
				print("Toggle Supply Timeout...")
				break

	elif state == 'off' or state == 'OFF':
		while "OK" not in str(out):
			ser.write(str('SOUT1\r\n').encode('UTF-8'))      # Port x OFF	
			time.sleep(1)
			out = ser.read(ser.inWaiting())
			# print("retry counter =" + str(retry))
			retry += 1
			if retry > 5:
				print("Toggle Supply Timeout...")
				break
	ser.close()         # close port

def powersupply_status(COM_Port):
	ser = serial.Serial(
	    port=COM_Port,
	    baudrate=9600,
	)
	ser.close()         # close port
	time.sleep(1)

	if(ser.isOpen() == False):
	    ser.open()
	    time.sleep(2)

	ser.write(str('GETD\r\n').encode('UTF-8'))      # Port x OFF
	out = ''
	# let's wait one second before reading output (let's give device time to answer)
	time.sleep(1)
	while ser.inWaiting() > 0:
		out += str(ser.read(1))
	print(out)
	ser.close()         # close port